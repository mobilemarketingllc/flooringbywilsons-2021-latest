<?php get_header(); ?>

<div class="fl-content-full container">
	<div class="row">
		<div class="fl-content col-md-12 black_bg">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					// get_template_part( 'content', 'page' );
$img = get_the_post_thumbnail_url(get_the_ID(), 'full')
					?>
					<div class='special_product_detail col-md-5'>
						<?php echo get_the_post_thumbnail( get_the_ID(), 'medium' );  ?>
						<h2><?php the_title();?></h2>
						<p class="special_plp_color"><?php echo get_field('color').' '.get_field('color_code');?></p>
						<p class="special_plp_price"><del><?php echo get_field('regular_price');?></del>  <span><?php echo get_field('sale_price'); ?></span></p>
						<a href="/about/location/" target="_self" class="fl-button" role="button">
							<span class="fl-button-text">VISIT US TODAY</span>
					</a>
					</div>
					<div class='special_product_form col-md-7'>
					     <h1 class="form-heading">Want to learn more about this product?</h1>
						 <p>Fill out the form below and we'll reach out to you shortly.</p>
						<?php echo do_shortcode('[gravityform id="22" title="false" description="false" ajax="true"]');;  ?>
					</div>
					<?php
				endwhile;
			endif;
			?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
